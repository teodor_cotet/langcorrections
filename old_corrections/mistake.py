from enum import Enum

class Mistake(Enum):
    TYPO = 'typo'
    INFLECTED = 'inflected'
    ALL = 'all'